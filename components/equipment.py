from equipment_slots import EquipmentSlots


class Equipment:
    def __init__(self, main_hand=None, off_hand=None, head=None, chest=None, gauntlets=None,cloak=None,feet=None,ranged_weapon=None,ammo=None):
        self.main_hand = main_hand
        self.off_hand = off_hand
        self.head = head
        self.chest = chest
        self.gauntlets = gauntlets
        self.cloak = cloak
        self.feet = feet
        self.ranged_weapon = ranged_weapon
        self.ammo = ammo

    @property
    def max_hp_bonus(self):
        bonus = 0

        
        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.max_hp_bonus
            
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.max_hp_bonus
            
        if self.head and self.head.equippable:
            bonus += self.head.equippable.max_hp_bonus
            
        if self.chest and self.chest.equippable:
            bonus += self.chest.equippable.max_hp_bonus
            
        if self.gauntlets and self.gauntlets.equippable:
            bonus += self.gauntlets.equippable.max_hp_bonus
            
        if self.cloak and self.cloak.equippable:
            bonus += self.cloak.equippable.max_hp_bonus
            
        if self.feet and self.feet.equippable:
            bonus += self.feet.equippable.max_hp_bonus
        
        if self.ranged_weapon and self.ranged_weapon.equippable:
            bonus += self.ranged_weapon.equippable.max_hp_bonus
            
        if self.ammo and self.ammo.equippable:
            bonus += self.ammo.equippable.max_hp_bonus
            
            
        return bonus

    @property
    def power_bonus(self):
        bonus = 0

        
        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.power_bonus
            
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.power_bonus
            
        if self.head and self.head.equippable:
            bonus += self.head.equippable.power_bonus
            
        if self.chest and self.chest.equippable:
            bonus += self.chest.equippable.power_bonus
            
        if self.gauntlets and self.gauntlets.equippable:
            bonus += self.gauntlets.equippable.power_bonus
            
        if self.cloak and self.cloak.equippable:
            bonus += self.cloak.equippable.power_bonus
            
        if self.feet and self.feet.equippable:
            bonus += self.feet.equippable.power_bonus
        
        if self.ranged_weapon and self.ranged_weapon.equippable:
            bonus += self.ranged_weapon.equippable.power_bonus
            
        if self.ammo and self.ammo.equippable:
            bonus += self.ammo.equippable.power_bonus
            

        return bonus

    @property
    def defense_bonus(self):
        bonus = 0

        
        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.defense_bonus
            
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.defense_bonus
            
        if self.head and self.head.equippable:
            bonus += self.head.equippable.defense_bonus
            
        if self.chest and self.chest.equippable:
            bonus += self.chest.equippable.defense_bonus
            
        if self.gauntlets and self.gauntlets.equippable:
            bonus += self.gauntlets.equippable.defense_bonus
            
        if self.cloak and self.cloak.equippable:
            bonus += self.cloak.equippable.defense_bonus
            
        if self.feet and self.feet.equippable:
            bonus += self.feet.equippable.defense_bonus
        
        if self.ranged_weapon and self.ranged_weapon.equippable:
            bonus += self.ranged_weapon.equippable.defense_bonus
            
        if self.ammo and self.ammo.equippable:
            bonus += self.ammo.equippable.defense_bonus
            

        return bonus


    @property
    def magic_bonus(self):
        bonus = 0

        
        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.magic_bonus
            
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.magic_bonus
            
        if self.head and self.head.equippable:
            bonus += self.head.equippable.magic_bonus
            
        if self.chest and self.chest.equippable:
            bonus += self.chest.equippable.magic_bonus
            
        if self.gauntlets and self.gauntlets.equippable:
            bonus += self.gauntlets.equippable.magic_bonus
            
        if self.cloak and self.cloak.equippable:
            bonus += self.cloak.equippable.magic_bonus
            
        if self.feet and self.feet.equippable:
            bonus += self.feet.equippable.magic_bonus
        
        if self.ranged_weapon and self.ranged_weapon.equippable:
            bonus += self.ranged_weapon.equippable.magic_bonus
            
        if self.ammo and self.ammo.equippable:
            bonus += self.ammo.equippable.magic_bonus
            

        return bonus

    def toggle_equip(self, equippable_entity):
        results = []

        slot = equippable_entity.equippable.slot

        if slot == EquipmentSlots.MAIN_HAND:
            if self.main_hand == equippable_entity:
                self.main_hand = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.main_hand:
                    results.append({"dequipped": self.main_hand})

                self.main_hand = equippable_entity
                results.append({"equipped": equippable_entity})
        elif slot == EquipmentSlots.OFF_HAND:
            if self.off_hand == equippable_entity:
                self.off_hand = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.off_hand:
                    results.append({"dequipped": self.off_hand})

                self.off_hand = equippable_entity
                results.append({"equipped": equippable_entity})
        elif slot == EquipmentSlots.HEAD:
            if self.head == equippable_entity:
                self.head = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.head:
                    results.append({"dequipped": self.head})

                self.head = equippable_entity
                results.append({"equipped": equippable_entity})
        elif slot == EquipmentSlots.CHEST:
            if self.chest == equippable_entity:
                self.chest = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.chest:
                    results.append({"dequipped": self.chest})

                self.chest = equippable_entity
                results.append({"equipped": equippable_entity})
        elif slot == EquipmentSlots.GAUNTLETS:
            if self.gauntlets == equippable_entity:
                self.gauntlets = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.gauntlets:
                    results.append({"dequipped": self.gauntlets})

                self.gauntlets = equippable_entity
                results.append({"equipped": equippable_entity})
        elif slot == EquipmentSlots.CLOAK:
            if self.cloak == equippable_entity:
                self.cloak = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.cloak:
                    results.append({"dequipped": self.cloak})

                self.cloak = equippable_entity
                results.append({"equipped": equippable_entity})
        elif slot == EquipmentSlots.FEET:
            if self.feet == equippable_entity:
                self.feet = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.feet:
                    results.append({"dequipped": self.feet})

                self.feet = equippable_entity
                results.append({"equipped": equippable_entity})
        elif slot == EquipmentSlots.RANGED_WEAPON:
            if self.ranged_weapon == equippable_entity:
                self.ranged_weapon = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.ranged_weapon:
                    results.append({"dequipped": self.ranged_weapon})

                self.ranged_weapon = equippable_entity
                results.append({"equipped": equippable_entity})
        elif slot == EquipmentSlots.AMMO:
            if self.ammo == equippable_entity:
                self.ammo = None
                results.append({"dequipped": equippable_entity})
            else:
                if self.ammo:
                    results.append({"dequipped": self.ammo})

                self.ammo = equippable_entity
                results.append({"equipped": equippable_entity})
        

        return results
