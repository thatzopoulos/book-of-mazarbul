from enum import Enum


class EquipmentSlots(Enum):
    MAIN_HAND = 1
    OFF_HAND = 2
    HEAD = 3
    CHEST = 4
    GAUNTLETS = 5
    CLOAK = 6
    FEET = 7
    RANGED_WEAPON = 8
    AMMO = 9
